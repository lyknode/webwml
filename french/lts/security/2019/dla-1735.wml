#use wml::debian::translation-check translation="fda7b127e9dcc32c4d37783a96b2168eeff7a2aa" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans rubygems imbriqué dans
ruby2.1, le langage de script interprété.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8320">CVE-2019-8320</a>

<p>Un problème de traversée de répertoire été découvert dans RubyGems. Avant de
créer de nouveaux répertoires ou fichiers (qui maintenant inclut le code de
vérification de chemin pour les liens symboliques), il supprime la destination
de la cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

<p>La commande owner de gem affiche le contenu de la réponse de l’API
directement sur la sortie standard. Par conséquent, si la réponse est
contrefaite, une injection de séquence d’échappement peut se produire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

<p>Gem::GemcutterUtilities#with_response peut afficher la réponse de l’API sur
la sortie standard telle quelle. Par conséquent, si le côté API modifie la
réponse, une injection de séquence d’échappement peut se produire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

<p>Un gem contrefait avec un nom multi-ligne n’est pas géré correctement. Par
conséquent, un attaquant pourrait injecter du code arbitraire sur la ligne de
souche de gemspec, qui est eval-ué par du code dans ensure_loadable_spec lors
la vérification de préinstallation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

<p>Un problème a été découvert dans RubyGems 2.6 et plus jusqu’à 3.0.2. Puisque
Gem::CommandManager#run appelle alert_error sans échappement, une injection de
séquence d’échappement est possible. (Il existe plusieurs façons de provoquer
cette erreur).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.5-2+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ruby2.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1735.data"
# $Id: $

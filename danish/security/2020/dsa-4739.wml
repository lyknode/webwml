#use wml::debian::translation-check translation="e0e83a446207444c8d7cbfe76be73fc5338ccab7" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9862">CVE-2020-9862</a>

    <p>Ophir Lojkine opdagede at kopiering af en URL fra Web Inspector, kunne 
    føre til kommandoindsprøjtning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9893">CVE-2020-9893</a>

    <p>0011 opdagede at en fjernangriber kunne være i stand til at forårsage en 
    uventet programafslutning eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9894">CVE-2020-9894</a>

    <p>0011 opdagede at en fjernangriber kunne være i stand til at forårsage en 
    uventet programafslutning eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9895">CVE-2020-9895</a>

    <p>Wen Xu opdagede at en fjernangriber kunne være i stand til at forårsage 
    en uventet programafslutning eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9915">CVE-2020-9915</a>

    <p>Ayoub Ait Elmokhtar opdagede at behandling af ondsindet fremstillet 
    webindhold kunne forhindre Content Security Policy i at blive 
    håndhævet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9925">CVE-2020-9925</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til universel udførelse af skripter på tværs af 
    servere.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.28.4-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4739.data"

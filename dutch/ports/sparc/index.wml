#use wml::debian::template title="Debian voor SPARC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h1>Debian geschikt gemaakt voor SPARC</h1>

<ul>
 <li><a href="#intro">Overzicht</a></li>
 <li><a href="#status">Huidige toestand</a></li>
 <li><a href="#sparc64bit">Over ondersteuning voor 64-bits SPARC</a>
 <ul>
   <li><a href="#kernelsun4u">Een kernel compileren voor sun4u</a></li>
 </ul></li>
 <li><a href="#errata">Errata</a></li>
 <li><a href="#who">Wie zijn wij? Hoe kan ik helpen?</a></li>
 <li><a href="#links">Waar kan ik meer informatie vinden?</a></li>
</ul>

<h2 id="intro">Overzicht</h2>
  <p>
Deze pagina's zijn bedoeld om gebruikers en ontwikkelaars van Debian die
Debian GNU/Linux gebruiken op de SPARC-architectuur, bij te staan. Op deze
pagina's vindt u informatie over de huidige toestand, over momenteel bekende
problemen, informatie voor en over ontwikkelaars die Debian geschikt maken
voor deze architectuur en verwijzingen naar bijkomende informatie.
  </p>

<h2 id="status">Huidige toestand</h2>
  <p>
Debian SPARC is officieel vrijgegeven en staat bekend als stabiel. Ondersteunde
machines zijn sun4u en sun4v (met een 32-bits gebruikersruimte). Raadpleeg de
<a href="../../releases/stable/sparc/">Installatiehandleiding</a>
voor informatie over ondersteunde systemen en hardware en over hoe Debian
geïnstalleerd kan worden.
  </p>


<h2 id="sparc64bit">Over ondersteuning voor 64-bits SPARC</h2>
  <p>
Zoals hierboven vermeld, ondersteunt de versie van Debian voor SPARC
de architecturen sun4u (<q>Ultra</q>) en sun4v (Niagara CPU).
Ze gebruikt een 64-bits kernel (gecompileerd met gcc 3.3 of recenter),
maar de meeste toepassingen worden in 32-bit uitgevoerd. Dit noemt men
ook een <q>32-bits gebruikersruimte</q> (userland).
  </p>
  <p>
De inspanningen in verband met Debian SPARC 64 (ook gekend als
<q>UltraLinux</q>) worden momenteel niet beschouwd als een volwaardige
inspanning om Debian geschikt te maken voor een bepaalde architectuur, zoals
dat voor andere architecturen het geval is. Ze zijn eerder bedoeld als een
<em>uitbreiding</em> van de Debian-versie voor SPARC.
  </p>
  <p>
In feite heeft het geen zin om alle applicaties in 64-bits modus uit te voeren.
Een volledige 64-bits modus brengt een aanzienlijke overhead (geheugen en
schijfopslag) met zich mee, vaak zonder dat dit voordeel oplevert. Sommige
toepassingen halen er echt voordeel uit als ze in 64-bits modus uitgevoerd
worden, en dat is de bedoeling van dit geschiktmakingswerk.
  </p>

<h3 id="kernelsun4u">Een kernel compileren voor sun4u</h3>
  <p>
Om een Linux-kernel voor Sun4u te compileren, moet u de broncodeboom van
Linux 2.2 of recenter gebruiken.
  </p>
  <p>
We raden sterk aan om ook het pakket <tt>kernel-package</tt> te gebruiken
als hulp bij de installatie en het beheer van kernels. U kunt een
geconfigureerde kernel compileren met één commando (als systeembeheerder):
  </p>
<pre>
  make-kpkg --subarch=sun4u --arch_in_name --revision=custom.1 kernel_image
</pre>


<h2 id="errata">Errata</h2>
  <p>
Enkele veelvoorkomende problemen inclusief reparaties of tijdelijke
oplossingen, zijn te vinden op onze <a href="problems">errata-pagina</a>.
  </p>


<h2 id="who">Wie zijn wij? Hoe kan ik helpen?</h2>
  <p>
Het geschikt maken van Debian voor SPARC is een gedeelde inspanning, net zoals
Debian dat is. Talloze mensen hielpen bij het geschiktmakingswerk en het
schrijven van documentatie, al bestaat er wel een beperkte lijst met
<a href="credits">personen die we erkentelijk zijn</a>.
  </p>
  <p>
Indien u graag wilt helpen, teken dan in op de mailinglijst
&lt;debian-sparc@lists.debian.org&gt;, zoals <a href="#links">hieronder
beschreven wordt</a>, en meld u aan.
  </p>
  <p>
Geregistreerde ontwikkelaars die graag actief willen meewerken aan het geschikt
maken van pakketten en het uploaden van deze geschikt gemaakte pakketten,
zouden de geschiktmakingsrichtlijnen uit de
<a href="$(DOC)/developers-reference/">Referentiehandleiding voor
ontwikkelaars</a> moeten lezen en de <a href="porting">pagina over het geschikt
maken van Debian voor SPARC</a> moeten raadplegen.
  </p>


<h2 id="links">Waar kan ik meer informatie vinden?</h2>
  <p>
De beste plaats om Debian-specifieke vragen te stellen over Debian voor SPARC
is de mailinglijst <a href="https://lists.debian.org/debian-sparc/">&lt;debian-sparc@lists.debian.org&gt;</a>.
<a href="https://lists.debian.org/debian-sparc/">Archieven</a> van de
mailinglijst kunnen doorbladerd worden op het web.
  </p>
  <p>
Om in te tekenen op de lijst, moet u een e-mail sturen naar
<a href="mailto:debian-sparc-request@lists.debian.org">\
debian-sparc-request@lists.debian.org</a>, met het woord `subscribe'
in de onderwerpregel, maar zonder inhoud. Een andere mogelijkheid is op het web
intekenen via de pagina voor het
<a href="https://lists.debian.org/debian-sparc/">intekenen op de mailinglijst</a>.
  </p>
  <p>
Vragen in verband met de kernel moeten gericht worden aan de lijst
&lt;sparclinux@vger.rutgers.edu&gt;. Intekenen gebeurt door het sturen van
een bericht met als inhoud <q>subscribe sparclinux</q> naar het adres <a
href="mailto:majordomo@vger.rutgers.edu">majordomo@vger.rutgers.edu</a>.
Er bestaat natuurlijk ook een Red Hat lijst.
  </p>
  <p>
Hier volgt een kleine lijst van links in verband met Linux SPARC
(ook gekend als <q>S/Linux</q>):
  </p>
 <ul>
      <li>
<a href="http://www.ultralinux.org/">UltraLinux</a> -- de
definitieve bron voor het geschikt maken van de kernel. Laat u niet
misleiden door de naam; meestal handelt de site gewoon over SPARC eerder
dan over UltraSPARC.</li>
 </ul>


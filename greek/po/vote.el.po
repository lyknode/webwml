# galaxico <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-25 22:16+0200\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: English <debian-www@lists.debian.org>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Ημερομηνία"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Χρονογραμμή"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Περίληψη"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Υποψηφιότητες"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Αποσύρσεις"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Δημόσια Αντιπαράθεση"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Πλατφόρμες (προγράμματα)"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Προτείνων/Προτείνουσα"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Πρόταση Α Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Πρόταση Β Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Πρόταση C Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Πρόταση D Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Πρόταση Ε Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Πρόταση F Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Πρόταση Α Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Πρόταση Α Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Μάρτυρες"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Μάρτυρες της Πρότασης Α"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Μάρτυρες της Πρότασης Β"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Μάρτυρες της Πρότασης C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Μάρτυρες της Πρότασης D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Μάρτυρες της Πρότασης E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Μάρτυρες της Πρότασης F"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Μάρτυρες της Πρότασης Α"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Μάρτυρες της Πρότασης Α"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Αντίθεση"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Κείμενο"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Πρόταση Α"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Πρόταση B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Πρόταση C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Πρόταση D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Πρόταση E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Πρόταση F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Πρόταση Α"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Πρόταση Α"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Επιλογές"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Τροπολογία Προτείνων/ουσα"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Μάρτυρες Τροπολογίας"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Τροπολογία Κείμενο"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Τροπολογία Προτείνων/ουσα Α"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Μάρτυρες Τροπολογίας Α"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Τροπολογία Κείμενο Α"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Τροπολογία Προτείνων/ουσα Β"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Μάρτυρες Τροπολογίας Β"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Τροπολογία Κείμενο Β"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Τροπολογία Προτείνων/ουσα C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Μάρτυρες Τροπολογίας C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Τροπολογία Κείμενο C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Τροπολογίες"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Πρακτικά"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Απαίτηση Πλειοψηφίας"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Δεδομένα και Στατιστικά"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Απαρτία"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Ελάχιστη Συζήτηση"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Ψηφοφορία"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Φόρουμ"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Αποτέλεσμα"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Σε αναμονή χορηγών"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Σε εξέλιξη"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Έχει αρχίσει ψηφοφορία"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Έχει αποφασιστεί"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "αποσύρθηκε"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Άλλα"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Κεντρική σελίδα ψηφοφορίας"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Πώς να ψηφίσετε"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Κάντε τις προτάσεις σας"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Πρόταση τροπολογίας"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Ακολουθία πρότασης"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Ανάγνωση αποτελεσμάτων"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Ψηφοφορία"

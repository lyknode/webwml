#use wml::debian::template title="Instalador do Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="27016443323dac06f67403e3c8fddd149558e1e2" maintainer="Fabio August De Muzio Tobich"

<h1>Notícias</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Notícias mais antigas</a>
</p>

<h1>Instalando com o Instalador do Debian (Debian-Installer)</h1>


<p>
<if-stable-release release="buster">
<strong>Para mídias oficiais de instalação e informações sobre o Debian
<current_release_buster></strong>, veja
<a href="$(HOME)/releases/buster/debian-installer">a página do
buster</a>.
</if-stable-release>
<if-stable-release release="bullseye">
<strong>Para mídias oficiais de instalação e informações sobre o Debian
<current_release_bullseye></strong>, veja
<a href="$(HOME)/releases/bullseye/debian-installer">a página do bullseye</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Todos os links das imagens abaixo são para a versão em desenvolvimento
do Instalador do Debian para o próximo lançamento do Debian e irão instalar,
por padrão, o Debian testing (<q><current_testing_name></q>).
</p>
</div>

<if-stable-release release="buster">
<p>

<strong>Para instalar o Debian testing</strong>, recomendamos que
use a versão <strong><humanversion /></strong> do instalador, após verificar sua
<a href="errata">errata</a>. As imagens a seguir estão disponíveis para a
versão <humanversion />:
<!--
<strong>Para instalar o Debian testing</strong>, recomendamos que
use as <strong>construções diárias</strong> do instalador. As seguintes imagens
estão disponíveis para as construções diárias:
-->
</p>

<h2>Versão oficial</h2>

<div class="line">
<div class="item col50">
<strong>imagens de CD netinst (geralmente 180-450 MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>outras imagens (netboot, pendrive USB, etc)</strong>
<other-images />
</div>
</div>

<p>
Ou instale a <b>imagem semanal atual do Debian testing</b>
que utiliza a mesma versão do instalador usada no último lançamento:
</p>

<h2>Snapshots semanais atuais</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>


</if-stable-release>
<!--
<p>
Se você preferir utilizar o último e maior, tanto para nos ajudar a testar o
futuro lançamento do instalador como por causa de problemas de hardware ou
outras questões, tente uma destas <strong>imagens construídas diariamente</strong>
que contêm a última versão disponível dos componentes do instalador.
</p>
-->

<h2>Snapshots diários atuais</h2>

<div class="line">
<div class="item col50">
<strong>imagens de CD netinst (geralmente 150-280 MB) <!-- e businesscard
(geralmente 20-50 MB) --></strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>imagens de CD netinst <!-- e businesscard -->
(via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>imagens de CD netinst multi-arch</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>outras imagens (netboot, pendrive USB, etc)</strong>
<devel-other-images />
</div>
</div>

<p>
Se algum hardware no seu sistema <strong>requer que um firmware seja
carregado</strong> com o driver do dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">\
arquivos compactados de pacotes de firmware comuns</a>. Instruções sobre como
usar os arquivos compactados e informações gerais sobre carregamento de
firmware durante uma instalação podem ser encontradas no Guia de Instalação
(veja a documentação abaixo).
</p>

<p>
<strong>Notas</strong>
</p>
<ul>
#	<li>Antes de baixar as imagens que são construídas diariamente, sugerimos
#	que verifique os <a href="https://wiki.debian.org/DebianInstaller/Today">
#	problemas conhecidos</a>.</li>
	<li>Uma arquitetura pode ser (temporariamente) omitida do resumo de
	imagens diárias se a mesma não estiver (confiavelmente) disponível.</li>
	<li>Para imagens de instalação, arquivos de verificação (<tt>SHA512SUMS</tt> e
  <tt>SHA256SUMS</tt>) estão disponíveis no mesmo diretório das imagens.</li>
  <li>Para baixar imagens completas de CD e DVD a utilização do jigdo é
  recomendada.</li>
  <li>Apenas um número limitado do conjunto completo de imagens de DVD estão
  disponíveis como arquivo ISO para baixar diretamente. A maioria dos usuários
  não precisa de todo software disponível em todos os discos, então para
  economizar espaço nos servidores e espelhos os conjunto completos estão
  disponíveis apenas via jidgo.</li>
	<li>As imagens de <em>CD netinst</em> multi-arch suportam i386/amd64; a
	instalação é similar à instalação a partir de uma imagem netinst de uma
  única arquitetura.</li>
</ul>

<p>
<strong>Depois de usar o Instalador do Debian</strong>, por favor, envie-nos um
<a href="https://d-i.debian.org/manual/pt.amd64/ch05s04.html#submit-bug">
relatório de instalação</a>, mesmo que não hajam problemas.
</p>

<h1>Documentação</h1>

<p>
<strong>Se você lê somente um documento</strong> antes de instalar, leia
nosso <a href="https://d-i.debian.org/manual/pt.amd64/apa.html">Howto de
Instalação</a>, uma passagem rápida pelo processo de instalação. Outras
documentações úteis incluem:
</p>

<ul>
<li>Guia de Instalação:
#    <a href="$(HOME)/releases/stable/installmanual">versão para o lançamento
#    atual</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">versão em desenvolvimento
    (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">última versão (Git)</a>
<br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do Instalador
do Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do Debian-CD</a><br />
perguntas mais frequentes e suas respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki do Instalador do
Debian</a><br /> documentação mantida pela comunidade</li>
</ul>

<h1>Entrando em contato conosco</h1>

<p>
A <a href="https://lists.debian.org/debian-boot/">lista de discussão debian-boot</a>
é o fórum principal para discussão e trabalho no Instalador do Debian.
</p>

<p>
Também temos um canal no IRC, #debian-boot em <tt>irc.debian.org</tt>. Esse
canal é utilizado principalmente para desenvolvimento, mas ocasionalmente
para suporte. Se você não receber uma resposta, tente a lista de discussão.
</p>

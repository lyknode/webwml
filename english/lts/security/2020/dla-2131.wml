<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple format string vulnerabilities in RRDtool, as used in Zenoss Core
before 4.2.5 and other products, allow remote attackers to execute arbitrary
code or cause a denial of service (application crash) via a crafted third
argument to the rrdtool.graph function, aka ZEN-15415, a related issue to
<a href="https://security-tracker.debian.org/tracker/CVE-2013-2131">CVE-2013-2131</a>.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4.8-1.2+deb8u1.</p>

<p>We recommend that you upgrade your rrdtool packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2131.data"
# $Id: $

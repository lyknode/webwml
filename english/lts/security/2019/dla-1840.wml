<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A flaw was found in the amd64 implementation of salsa20. If more
than 256 GiB of keystream is generated, or if the counter otherwise
grows greater than 32 bits, the amd64 implementation will first generate
incorrect output, and then cycle back to previously generated keystream.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.0~hg190-1+deb8u1.</p>

<p>obfs4proxy has been rebuilt as version 0.0.3-2+deb8u1.</p>

<p>We recommend that you upgrade your golang-golang-x-crypto-dev
and obfs4proxy packages, and rebuild any software using
golang-golang-x-crypto-dev.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1840.data"
# $Id: $

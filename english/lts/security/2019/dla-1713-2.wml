<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of libsdl1.2 released as DLA 1713-1 led to a regression, caused
by an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>. This issue was known upstream and
resulted, among others, in windows versions from libsdl1.2 failing to set
video mode.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.15-10+deb8u2.</p>

<p>We recommend that you upgrade your libsdl1.2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1713-2.data"
# $Id: $

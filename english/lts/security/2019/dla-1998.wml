<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were multiple double free
vulnerabilities in python-psutil, a Python module providing
convenience functions for accessing system process data.</p>

<p>This was caused by incorrect reference counting handling within
for/while loops that convert system data into said Python objects.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18874">CVE-2019-18874</a>

    <p>psutil (aka python-psutil) through 5.6.5 can have a double free. This occurs because of refcount mishandling within a while or for loop that converts system data into a Python object.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.1-1+deb8u1.</p>

<p>We recommend that you upgrade your python-psutil packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1998.data"
# $Id: $
